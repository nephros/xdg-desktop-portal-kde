%global qt_version 5.15.8
%global kf_version 5.108.0
%global pname xdg-desktop-portal-kde
Name:       xdg-desktop-portal-kde-mini
Summary:    XDG Desktop Portal for KDE
Version:    5.27.9
Release:    0
Group:      Applications
License:    BSD 2-Clause and LGPLv2+ and LGPLv2.1 and LGPLv3 and GPLv2+
URL:        https://invent.kde.org/plasma/xdg-desktop-portal-kde
Source0:    %{name}-%{version}.tar.bz2
Requires:   pipewire >= 0.3
BuildRequires:  pkgconfig(systemd)
BuildRequires:  pkgconfig(dbus-1)
#BuildRequires:  pkgconfig(glib-2.0)
#BuildRequires:  pkgconfig(json-glib-1.0)
#BuildRequires:  pkgconfig(gdk-pixbuf-2.0)
#BuildRequires:  pkgconfig(libpipewire-0.3)
BuildRequires:  pkgconfig
BuildRequires:  cmake

BuildRequires: opt-kf5-rpm-macros >= 5.11
BuildRequires: opt-qt5-rpm-macros >= %{qt_version}
BuildRequires: opt-extra-cmake-modules >= %{qt_version}

# KF5: CoreAddons Config I18n Declarative GuiAddons
# GlobalAccel KIO Kirigami2 Notifications Plasma Service Wayland
# WidgetsAddons WindowSystem IconThemes) (Required is at least version

BuildRequires: opt-kf5-kconfig-devel
BuildRequires: opt-kf5-kcoreaddons-devel
BuildRequires: opt-kf5-kguiaddons-devel
BuildRequires: opt-kf5-kio-devel
BuildRequires: opt-kf5-kiconthemes-devel
BuildRequires: opt-kf5-kirigami-addons
BuildRequires: opt-kf5-kirigami2-devel
BuildRequires: opt-kf5-knotifications-devel
BuildRequires: opt-kf5-kwayland-devel
BuildRequires: opt-kf5-kwidgetsaddons-devel
BuildRequires: opt-kf5-kwindowsystem-devel
BuildRequires: opt-kf5-ki18n-devel
BuildRequires: opt-plasma-integration-devel

BuildRequires: opt-qt5-qtbase-devel >= %{qt_version}
BuildRequires: opt-qt5-qtbase-static >= %{qt_version}
BuildRequires: opt-qt5-qtbase-private-devel
%{?_opt_qt5:Requires: %{_opt_qt5}%{?_isa} = %{_opt_qt5_version}}
#BuildRequires: opt-qt5-qtbase-gui-devel >= %{qt_version}
BuildRequires: opt-qt5-qtsvg-devel >= %{qt_version}
BuildRequires: opt-qt5-qtdeclarative-devel >= %{qt_version}
BuildRequires: opt-qt5-qtwayland-devel >= %{qt_version}
BuildRequires: libxkbcommon-devel

%opt_qt5_default_filter
%opt_kf5_default_filter
Requires: opt-qt5-qtbase-gui >= %{qt_version}
Requires: qt-runner
Requires: breeze-icons

Requires: xdg-desktop-portal
Conflicts: xdg-desktop-portal-kde
#Provides: xdg-desktop-portal-kde

%description
%{summary}.
%if "%{?vendor}" == "chum"
Title: %{summary}
PackagedBy: nephros
Categories:
 - Audio
 - Video
Custom:
  Repo: %{url}
Links:
  Homepage: %{url}
%endif

%prep
%setup -q -n %{name}-%{version}

%build
export QTDIR=%{_opt_qt5_prefix}

%_opt_cmake_kf5 \
   -DCMAKE_INSTALL_PREFIX:PATH=%{_prefix} \
  -DKDE_INSTALL_LIBEXECDIR:PATH=%{_libexecdir} \
  -DKDE_INSTALL_LIBDIR:PATH=%{_libdir} \
  -DKDE_INSTALL_DATAROOTDIR:PATH=%{_datadir} \
  -DCMAKE_INSTALL_LIBEXECDIR:PATH=%{_libexecdir} \
  -DCMAKE_INSTALL_LIBDIR:PATH=%{_libdir} \
  -DCMAKE_INSTALL_DATAROOTDIR:PATH=%{_datadir}

%make_build


%install
rm -rf %{buildroot}
%make_install


%files
%defattr(-,root,root,-)
%{_libexecdir}/*
%{_userunitdir}/*
%{_datadir}/dbus-1/*
#%%{_datadir}/%{pname}
%{_datadir}/xdg-desktop-portal/portals/kde.portal
%exclude %{_datadir}/xdg-desktop-portal/kde-portals.conf
%exclude %{_datadir}/applications/*
%exclude %{_datadir}/qlogging-categories5/*
%{_datadir}/locale
# >> files
# << files

