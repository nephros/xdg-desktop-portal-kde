/*
 * SPDX-FileCopyrightText: 2016 Red Hat Inc
 *
 * SPDX-License-Identifier: LGPL-2.0-or-later
 *
 * SPDX-FileCopyrightText: 2016 Jan Grulich <jgrulich@redhat.com>
 */

#include "desktopportal.h"
#include "desktopportal_debug.h"

DesktopPortal::DesktopPortal(QObject *parent)
    : QObject(parent)
    , m_settings(new SettingsPortal(this))
{
}

DesktopPortal::~DesktopPortal()
{
}
